from open_file import open_file

def input_date(message, method):
    if method == 'add':
        date = input(message)
        return date
    dictionary = {}
    dictionary = open_file('r')
    date = input(message)
    if date in dictionary.keys():
        return date
    else:
        print(f'Вы пытаетесь {method} событие на {date}. Но в этот день у вас нет никаких событий')
        return input_date(message, method)
            

def input_time(message, data, method):
    if method == 'add':
        time = input(message)
        if not data:
            return time
        elif time in data.keys():
            print(f'В {time} у вас уже есть запись {data.get(time)}. Выбирите другое время')
            return input_time(message, data, method)
        else:
            return time
    time = input(message)
    if time not in data.keys():
        print(f'Вы пытаетесь {method} событие в {time}. Но в это время у вас нет события')
        return input_time(message, data, method)
    else:
        return time