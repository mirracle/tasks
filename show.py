from open_file import open_file

def show_task():
    date = input('Введите дату (пример 01.01.2020): ')
    dictionary = open_file('r')
    message = f'Событий на дату {date} не найдено!'
    task = dictionary.get(date)
    if task:
        message = [f'{a} - {b}' for a, b in task.items()]
        for item in message:
            print(item)
        return None
    print(message)

def show_all_tasks():
    dictionary = open_file('r')
    message = [f'{a} - {c} - {d}' for a, b in dictionary.items() for c, d in b.items()]
    for item in sorted(message):
        print(item)