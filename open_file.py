import ast

def open_file(method, data=None):
    if method == 'r':
        with open('data.txt', 'r') as file:
            return ast.literal_eval(file.read())
    if method == 'w':
        with open('data.txt', 'w') as file:
            file.write(str(data))