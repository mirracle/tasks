from open_file import open_file
from inputs import input_time, input_date
from menu import accept

def update_task():
    dictionary = open_file('r')

    old_date = input_date(
        'Введите дату в которой вы хотите поменять событие (пример 01.01.2020): ', 
        'изменить'
    )
    old_time = input_time(
        'Введите время в которое вы хотетие поменять событие (пример 14:00): ', 
        dictionary.get(old_date), 
        'изменить'
    )
    old_description = dictionary.get(old_date).get(old_time)
    print(f'{old_date} в {old_time} у вас есть событие {old_description}. Введите для него новые значения')
    new_date = input_date(f'Введите новую дату. Или оставьте старую {old_date}: ', 'add')
    new_time = input(f'Введите новое вермя. Или оставьте старое {old_time}: ')
    new_description = input(f'Введите новое описание события Или оставте старую {old_description}: ')
    check = accept()
    if not check:
        return None
    dictionary.get(old_date).pop(old_time)
    if new_date in dictionary.keys():
        dictionary.get(new_date).update({new_time: new_description})
    else:
        task = {new_date: {new_time: new_description}}
        dictionary.update(task)
    open_file('w', data=dictionary)