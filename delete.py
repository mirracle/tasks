from open_file import open_file
from inputs import input_time, input_date
from menu import accept

def delete_task():
    dictionary = open_file('r')
    date = input_date('Введите дату в которой вы хотите удалить событие (пример 01.01.2020): ', 'удалить')
    time = input_time(
        'Введите время в которое вы хотетие удалить событие (пример 14:00): ',
        dictionary.get(date),
        'удалить'
    )
    check = accept()
    if not check:
        return None
    dictionary.get(date).pop(time)
    open_file('w', data=dictionary)