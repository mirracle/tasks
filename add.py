from open_file import open_file
from inputs import input_time, input_date

def add_task():
    dictionary = open_file('r')
    date = input_date('Введите дату (пример 01.01.2020): ', 'add')
    time = input_time(
        'Введите время (пример 14:00): ',
        dictionary.get(date),
        'add'
    )
    description = input('Введите описания события: ')
    if date in dictionary.keys():
        dictionary.get(date).update({time: description})
    else:
        task = {date: {time: description}}
        dictionary.update(task)
    open_file('w', data=dictionary)