from menu import print_menu
from add import add_task
from update import update_task
from delete import delete_task
from show import show_all_tasks, show_task

command = None

while True:
    print_menu()
    command = input('Введите номер действия: ')
    print('***********************************')
    if command == '1':
        add_task()
    elif command == '2':
        update_task()
    elif command == '3':
        delete_task()
    elif command == '4':
        show_task()
    elif command == '5':
        show_all_tasks()
    else:
        break
    